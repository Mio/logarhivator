<?php
/**
  +--------------------------------------------------------------------------
  |   Log database arhivator
  |   =============================================
  |   by Sultanov Denis aka DenSul aka Mio
  |   (c) 2014
  |   http://vk.com/programmers
  |   =============================================
  |   Web: http://devlock.ru
  |   Email: <sultanden@gmail.com>
  +---------------------------------------------------------------------------
  |   THIS FREE!
  +---------------------------------------------------------------------------
  |   > Главный контроллер
  |   @author Module written by Sultanov Denis
  |   @copyright Sultanov Denis Date started: 06.05.2014 12:58:12
  |   @license GNU GPL
  |   @package logArchiver
  |   @subpackage app
  |   @since Module Version Number: 0.1
  +--------------------------------------------------------------------------
*/

namespace logArchiver;

require_once 'system/database.php';
require_once 'system/archive.php';

use logArchiver\database;
use logArchiver\archive;
use DateTime;
use SimpleXMLElement;

class App {
    
    /**
     * @var    array
     * @access private
     */
    
    private $helper = array();
    
    /**
     * @var    object
     * @access private
     */
    
    private $db     = null;
    
    /**
     * @var    array
     * @access private 
     */
    
    private $settings = array(
        'path_conf'         => 'config/databases.xml',
        'notification'      => false,
        'email'             => 'sultanden@gmail.com',
        'insertLimit'       => 1000,
        'selectLimit'       => 5000,
    );
    
    /**
     * @param  array $settings
     * @return object class app
     */
    
    public function __construct($settings = false) { if ( $settings ) $this->settings = $settings; }
    
    /**
     * Инициализация приложения
     * Получаем конфиг, парсим его
     * Создаем подключения к базам данных
     * Вызываем архиватор
     * 
     * @param  void
     * @return object $this
     */
    
    public function initialization()
    {
        $configXML = new SimpleXMLElement('config/databases.xml', false, true);
        
        $databases           = array();
        $databases['master'] = (array) $configXML->databases->database[0];
        $databases['slave']  = (array) $configXML->databases->database[1];
        
        $this->db = new database($databases);
 
        for ($i = 0; $i < count($databases['master']['tables']); $i++)
        {
            $condition = $databases['master']['tables']->table[$i]->condition;
            $databases['master']['tables']->table[$i]->condition = $this->conditionReplace($condition);
        }
        
        $this->db->setTables($databases);
        
        return $this;
    }
    
    /**
     * Переопределяем условия
     * 
     * @param  string $string
     * @return sring
     * @access private
     */
    
    private function conditionReplace($string)
    {
        $string = str_replace("%less%", '<', $string);
        $string = str_replace("%larger%", '>', $string);
        $string = str_replace("%lessor%", '<=', $string);
        $string = str_replace("%largeror%", '>=', $string);
        
        return $string;
    }
    
    /**
     * Архивируем и очищаем логи
     * 
     * @param  mixed $completeCallback
     * @return void
     */
    
    public function run($completeCallback = false)
    {
        $archiver = new archive($this->db->getTables(), $this->settings, $this->helper);
        $archiver->runDataMaster($this->db);
        
        $reportDataArr = $archiver->generateReportData($this->db);
        $reportData    = $this->trigger('report', $reportDataArr);
        
        $this->trigger('notice', $reportData);
        $this->trigger('complete_display', $reportDataArr);
    }
    
    /**
     * @param   string $name
     * @param   mixed $args
     * @return  mixed
     */
    
    public function trigger($name, $args)
    {
        if ( !isset($this->helper[$name]) )
            return false;
        
        return $this->helper[$name]($args);
    }
    
    /**
     * @param  string $name
     * @param  object closure $function
     * @return \logArchiver\App
     */
    
    public function on($name, $function)
    {
        if ( !isset($this->helper[$name]) )
            $this->helper[$name] = $function;
        
        return $this;
    }
    
}

?>