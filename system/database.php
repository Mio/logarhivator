<?php
/**
  +--------------------------------------------------------------------------
  |   Log database arhivator
  |   =============================================
  |   by Sultanov Denis aka DenSul aka Mio
  |   (c) 2012 - 2014
  |   http://vk.com/programmers
  |   =============================================
  |   Web: http://devlock.ru
  |   Email: <sultanden@gmail.com>
  +---------------------------------------------------------------------------
  |   THIS FREE!
  +---------------------------------------------------------------------------
  |   > Подключение к базам данных
  |   @author Module written by Sultanov Denis
  |   @copyright Sultanov Denis Date started: 06.05.2014 12:58:12
  |   @license GNU GPL
  |   @package logArchiver
  |   @subpackage database
  |   @since Module Version Number: 0.1
  +--------------------------------------------------------------------------
*/

namespace logArchiver;

use PDO;

class database {
    
    /**
     * @var    array экземпляры класса PDO
     * @access private
     */
    
    private $db       = null;    
    
    /**
     * @var    integer кол. запросов
     * @access public
     */
    
    public $sqlcount  = 0;
    
    /**
     * @var    object Таблицы с условиями
     * @access xml object
     */
    
    private $tables   = null;
    
    /**
     * Инициализация класса.
     * Пробуем подключиться к базам
     * $config['master'] - Отсюда выкачиваем логи и очищаем
     * $config['slave'] - Место хранение логов
     * 
     * Хотел сделать переопределенный класс, но констрктор ничего не возвращал :(
     * 
     * @param  array $config
     * @return object class database
     */
    
    public function __construct(array $config) 
    {
        if ( !$config || !$config['master'] )
            throw new Exception ('Не найден конфигурационный файл');
        
        foreach ( $config as $key => $setting )
        {
            try {
                $this->db[$key] = new PDO($setting['adapter'] . ':host=' . $setting['hostname'] . ';port='.$setting['port'].';dbname=' . $setting['database'],
                                                       $setting['username'], $setting['password']);
                
                
                $this->db[$key]->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
                $this->db[$key]->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
                $this->db[$key]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $this->db[$key]->query('SET NAMES UTF8');

            } catch (PDOException $e)
            {
                echo 'PDO MASTER DB: ' . $e->getMessage();
            }
        }
    }
    
    /**
     * Устанавливаем таблицы
     * 
     * @param  object $tables
     * @return object $this
     * @access public
     */
    
    public function setTables($tables) {$this->tables = $tables; return $this;}
    
    /**
     * Отдаем таблички
     * 
     * @param  void
     * @return object
     * @access public
     */
    
    public function getTables() {return $this->tables;}
    
    /**
     * Выполнение запроса в нужном линке
     * 
     * @param  string $sql
     * @param  string $link
     * @return object PDOStatement or boolean
     */
    
    public function execute($sql, $link = 'master')
    {
        $this->sqlcount++;
        return $this->db[$link]->query($sql); 
    }
    
    /**
     * Выполнение запроса в нужном линке
     * 
     * @param  string $sql
     * @param  string $link
     * @return object PDOStatement or boolean
     */
    
    public function exec($sql, $link = 'master')
    {
        $this->sqlcount++;
        return $this->db[$link]->exec($sql); 
    }    
    
}

?>
