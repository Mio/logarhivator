<?php
/**
  +--------------------------------------------------------------------------
  |   Log database arhivator
  |   =============================================
  |   by Sultanov Denis aka DenSul aka Mio
  |   (c) 2012 - 2014
  |   http://vk.com/programmers
  |   =============================================
  |   Web: http://devlock.ru
  |   Email: <sultanden@gmail.com>
  +---------------------------------------------------------------------------
  |   THIS FREE!
  +---------------------------------------------------------------------------
  |   > Переносим логи
  |   @author Module written by Sultanov Denis
  |   @copyright Sultanov Denis Date started: 06.05.2014 12:58:12
  |   @license GNU GPL
  |   @package logArchiver
  |   @subpackage archive
  |   @since Module Version Number: 0.1
  +--------------------------------------------------------------------------
*/

namespace logArchiver;

use PDO;

class archive {
    
    /**
     * @var    object 
     * @access private
     */
    
    private $master = null;
    
    /**
     * @var    object 
     * @access private
     */    
    
    private $slave  = null;
    
    /**
     * @var    array
     * @access private
     */
    
    private $conf   = array();
    
    /**
     * @var    array
     * @access private
     */
    
    private $helper = array();    
    
    /**
     * @var    array
     * @access private
     */
    
    private $data   = array();
    
    /**
     * @var    array
     * @access private
     */
    
    private $files  = array();
    
    /**
     * Получаем таблицы с конфига
     * 
     * @param  array $tables
     * @param  array $conf
     * @return object class archive
     */
    
    public function __construct(array $tables, $conf, $helpers)
    {
        $this->master = $tables['master']['tables'];
        $this->slave  = $tables['slave']['tables'];
        $this->conf   = $conf;
        $this->helper = $helpers;
    }
    
    /**
     * Получаем данные с лимитом [master]
     * И записываем их в папку кэша, чтобы не грузить память
     * 
     * @param  string $link
     * @param  object database $db
     * @return object $this
     * @access public
     */
    
    public function runDataMaster(database $db)
    {
        foreach ( $this->master as $table )
        {
            $tablename = (string) $table['name'];
            $columns   = $this->getColumn($tablename);
            $condition = (string)$table->condition;
            $total     = $this->getCountDataMaster($db, $tablename, $condition);
            $num_pages = ceil($total/$this->conf['selectLimit']);
            
            $this->data['dataCount'] += $total;
            
            if ( !file_exists('cache/' . $tablename) )
              {
                mkdir('cache/' . $tablename, 0777);  
                
                $this->trigger('gm', 'В таблице: '. $tablename . ' записей: ' . $total);
                $this->trigger('gm', 'В таблице: '. $tablename . ' будет создано: ' . $num_pages . ' файлов кэша (serialize)');
                $this->trigger('gm', 'В каждом файле ' . $this->conf['selectLimit'] . ' инсертов');
                $this->trigger('wm', 'Начинаю сбор инфы!');
                sleep(5);

                for ( $i = 0; $i < $num_pages; $i++ )
                {
                    $sql  = 'SELECT '.$columns.' FROM ' . $tablename . ' WHERE ' . $condition . ' LIMIT ' . $i * $this->conf['selectLimit'] . ', ' . $this->conf['selectLimit']; 
                    $data = $this->generateSqlSlave($this->getTable($tablename, 'slave'), $db->execute($sql)->fetchAll(PDO::FETCH_OBJ));

                    $this->createCacheFile('cache/' . $tablename . '/' . $i, $data);
                    $this->files[$tablename][$i] += $i;
                }

                $this->trigger('gm', 'В таблице: ' . $tablename . ' всего файлов: ' . count($this->files[$tablename]));
                
                $this->trigger('wm', 'Начинаю удаление данные с мастера');
                sleep(5);

                $this->deleteMasterData($db, $tablename, $condition);

                $this->trigger('wm', 'Начинаю перенос данных');
                sleep(5);
                $this->dataTransferSlave($db, $tablename);  
                
              }
            else
              {
                $this->trigger('wm', 'Начинаю удаление данные с мастера');
                sleep(5);

                $this->deleteMasterData($db, $tablename, $condition);

                $this->trigger('wm', 'Начинаю перенос данных');
                sleep(5);
                $this->dataTransferSlave($db, $tablename);  
              }     
        }
        
        return $this;
    }
    
    /**
     * Получаем таблицу
     * 
     * @param  string $nametable
     * @param  string $link
     * @return object
     * @access public
     */
    
    public function getTable($nametable, $link)
    {
        foreach ( $this->$link as $table )
        {
            if ( (string) $table['name'] == $nametable )
              {
                return $table;  
              }
        }
    }
    
    /**
     * Генерируем SQL запрос для записи
     * 
     * @param  object $table
     * @param  array $data
     * @return boolean
     * @access private
     */
    
    private function generateSqlSlave($table, $data)
    {
        $columns       = (array)$table->columns;
        $columns_slave = @implode(',', $columns['column']);  
        $sql = 'INSERT INTO ' . (string) $table['name'] . ' ('.$columns_slave.') VALUES ';
        
        foreach ( $data as $key => $value )
        {
            $sql .= '(';
            foreach ( $columns as $_column )
            {
                foreach ( $_column as $column )
                    $sql .= '"'.$data[$key]->{$column}.'",';
                
                $sql = preg_replace("/,$/", '', $sql);
            }
            $sql .= '),';
        } 
        $sql = preg_replace("/,$/", ';', $sql);
        
        return $sql;
    }
    
    /**
     * Удаляем данные с мастера
     * 
     * @param  string $table
     * @return array
     * @access private
     */
    
    private function deleteMasterData($db, $table, $condition)
    {
        $query                    = 'DELETE FROM '.$table.' WHERE ' . $condition;
        $this->data['removeData'] = $db->exec($query, 'master');

        return $this;
    }
    
    /**
     * Создаем файл кэша
     * 
     * @param  string $path
     * @param  string $data
     * @return array
     */
    
    private function createCacheFile($path, $data)
    {
        $fp = fopen($path, "w+");
        fwrite($fp, $data);
        fclose($fp);
        chmod($path, 0750);
        $this->trigger('wm', 'Создан файл запросов: '. $path);
        sleep(1);
    }
    
    /**
     * Выполнение пользовательских тригеров
     * 
     * @param  string $helper
     * @param  mixed $args
     * @return 
     */
    
    private function trigger($helper, $args)
    {
        if ( isset($this->helper[$helper]) )
            $this->helper[$helper]($args);
    }
    
    /**
     * Получаем количество записей
     * 
     * @param \logArchiver\database $db
     * @param  string $table name
     * @param  string $condition
     * @return int
     * @accces private
     */
    
    private function getCountDataMaster(database $db, $table, $condition)
    {
        $sql = 'SELECT COUNT(*) as count FROM '.$table . ' WHERE ' . $condition;
        return $db->execute($sql)->fetch()->count;
    }
    
    /**
     * Получаем поля
     * 
     * @param  string $nametable
     * @param  string $link
     * @return string
     */
    
    public function getColumn($nametable, $link = 'master')
    {
        foreach ( $this->$link as $table )
        {
            if ( (string) $table['name'] == $nametable )
              {
                $columns = (array)$table->columns;
                return @implode(',', $columns['column']);  
              }
        }
    
    }
    
    /**
     * Создаем массив с данными
     * 
     * @param  database object
     * @return array
     * @access public
     */
    
    public function generateReportData(database $db)
    {
        global $time;
        
        $timed = microtime(true) - $time;
        
        return array('sqlcount' => $db->sqlcount, 'memory' => memory_get_peak_usage(true), 
                     'files' => $this->data['files_count'], 'time' => $timed, 'dataCount' => $this->data['dataCount']);
    }
    
    /**
     * Переносим данные на архив
     * 
     * @param \logArchiver\database $db
     * @param string $tablename
     * @return mixed
     * @access public
     */
    
    public function dataTransferSlave(database $db, $tablename)
    {
        $this->data['files_count'] = 0;
        $dir = opendir(realpath('cache/' . $tablename));
        $count = 0;
        while (false !== ($file = readdir($dir))) {
            if($file == '.' || $file == '..' || is_dir('cache/' . $tablename . '/' . $file) ){
                continue;
            }
            
            $sql    = file_get_contents('cache/' . $tablename . '/' . $file, true);
            $result = $db->exec($sql, 'slave');
            $this->trigger('gm', $result . ' Выполнено!');
            $count++;
            
            @unlink('cache/' . $tablename . '/' . $file);
            $this->files[$tablename][$i] = $count;
            $this->data['files_count']++;
        }
        
        rmdir('cache/' . $tablename);
        sleep(2);
    }
    
}
?>
